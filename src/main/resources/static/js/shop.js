const menu = document.getElementById("categoriesMenu");
Array.from(menu.children).forEach((element)=>{
    element.setAttribute("draggable", "false");
}
);
let mouseDown = false;
let startX, scrollLeft;
let wasDragged = false;

const startDragging = function(event) {
    mouseDown = true;
    startX = event.pageX - menu.offsetLeft;
    scrollLeft = menu.scrollLeft;
    wasDragged = false;
};
const stopDragging = function(event) {
    mouseDown = false;
};

const clickAfterDragging = function(event) {
    if (wasDragged) {
        event.preventDefault();
    }
};
let categoriesMenuIsHorizontal = null;
const windowResize = function(event) {
    let mode = window.innerWidth < 768;
    if (mode === categoriesMenuIsHorizontal)
        return;
    categoriesMenuIsHorizontal = mode;
    const header = document.querySelector("header");
    header.appendChild(menu);
    if (categoriesMenuIsHorizontal) {} else {
        const main = document.querySelector("main");
        main.parentNode.insertBefore(menu, main);
    }
};
windowResize();

menu.addEventListener("mousemove", (e)=>{
    e.preventDefault();
    if (!mouseDown) {
        return;
    }
    wasDragged = true;
    const x = e.pageX - menu.offsetLeft;
    const scroll = x - startX;
    menu.scrollLeft = scrollLeft - scroll;
}
);

// Add the event listeners
menu.addEventListener("click", clickAfterDragging, false);
menu.addEventListener("mousedown", startDragging, false);
menu.addEventListener("mouseup", stopDragging, false);
menu.addEventListener("mouseleave", stopDragging, false);

window.addEventListener("resize", windowResize, false);

const cartBadge = document.getElementById("cartBadge");
const cartIcon = document.getElementById("shoppingCartIcon");

const currentCategory = window.location.pathname.match(/\/shop\/(?<category>[^/]+)/)?.groups.category;

function addToCart(event) {
    let cardNode = event.target;
    ``
    let itemId;
    do {
        cardNode = cardNode.parentNode;
        itemId = cardNode.id && cardNode.id.match(/item(?<id>\d+)/);
        itemId = itemId && itemId.groups.id;
    } while (!itemId);
    let itemName = document.querySelector(`#item${itemId} .card-title`).innerHTML;
    let itemPrice = document.querySelector(`#item${itemId} .price`).innerHTML;
    const itemImg = document.querySelector(`#item${itemId} img`)?.attributes?.src?.value;
    let shop = sessionStorage.getItem("shop");
    shop = (shop && JSON.parse(shop)) || {
        cart: [],
        cartCounter: 0,
    };

    let entry = shop.cart.find((entry)=>entry.id === itemId);
    if (!entry) {
        entry = {
            id: itemId,
            price: itemPrice,
            name: itemName,
            category: currentCategory,
            url: `/shop/${currentCategory.toLocaleLowerCase()}#item${itemId}`,
            imgUrl: itemImg,
            amount: 0,
        };
        shop.cart.push(entry);
    }
    entry.amount++;
    shop.cartCounter++;
    cartBadge.textContent = shop.cartCounter;
    sessionStorage.setItem("shop", JSON.stringify(shop));
    if (shop.cartCounter > 0) {
        let shoppingCart = document.getElementById("shoppingCart");
        shoppingCart.classList.remove("disabled");
    }
    shoppingCartIcon.classList.remove("tada");
    void shoppingCartIcon.offsetWidth;
    shoppingCartIcon.classList.add("tada");
}

function initCartButtons() {
    let btns = document.querySelectorAll(".card button");
    btns.forEach((btn)=>{
        btn.addEventListener("click", addToCart);
    }
    );
}
initCartButtons();


