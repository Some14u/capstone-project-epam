function assignCartToForm() {
    let shop = sessionStorage.getItem("shop");
    shop = (shop && JSON.parse(shop)) || { cart: [], cartCounter: 0 };
    const dataNode = document.getElementById("cartDataPlaceholder");
    dataNode.value = shop.cart.map(({id, amount}) => id + "-" + amount).join(",");
}

function assignCartCleanerOnFormSubmit() {
    const form = document.getElementById("cartForm");
    form.addEventListener("submit", cartCleaner, false);
}

function cartCleaner() {
    sessionStorage.removeItem("shop");
}

assignCartToForm();

assignCartCleanerOnFormSubmit();