  function initShopButton() {
    let shop = sessionStorage.getItem("shop");
    shop = (shop && JSON.parse(shop)) || { cart: [], cartCounter: 0 };
    if (shop.cartCounter > 0) {
      const shoppingCart = document.getElementById("shoppingCart");
      const cartBadge = document.getElementById("cartBadge");
      cartBadge.textContent = shop.cartCounter;
      shoppingCart.classList.remove("disabled");
    }
  }

  initShopButton();