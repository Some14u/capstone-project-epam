      let shop = sessionStorage.getItem("shop");
      shop = shop && JSON.parse(shop) || { cart: [], cartCounter: 0 };
      if (!shop || !shop.cart) {
        if (window.history.length <= 2) {
          window.location.href = "/shop";
        } else {
          window.history.back();
        }
      }

      fillForm();
      assignQuantityButtons();
      assignDeleteButtons();

      function assignQuantityButtons() {
        const buttons = document.querySelectorAll(".item-quantity > button");
        buttons.forEach((button) => {
          const id = button.dataset.itemId;
          const record = document.getElementById(`item${id}`);
          const input = document.querySelector(`input[name="quantity-item${id}"]`);
          const cartItem = shop.cart.find((item) => item.id === id);
          const action = button.dataset.action;
          const priceRelative = document.querySelector(`#item${id} .price-relative`);
          const totalPrice = document.querySelector("#total-price span");
          const shoppingCart = document.getElementById("shoppingCart");
          const cartBadge = document.getElementById("cartBadge");
          button.addEventListener("click", () => {
            if (action === "minus" && cartItem.amount === 0) return;
            cartItem.amount = cartItem.amount + (action === "minus" ? -1 : 1);
            shop.cartCounter = shop.cartCounter + (action === "minus" ? -1 : 1);
            sessionStorage.setItem("shop", JSON.stringify(shop));
            input.value = cartItem.amount;
            input.dispatchEvent(new Event("change"));
          });
          input.addEventListener("change", (event) => {
            event.stopImmediatePropagation()
            const value = event.target.value;
            priceRelative.innerHTML = cartItem.amount * cartItem.price;
            const total = shop.cart.reduce((acc, item) => acc + item.amount * item.price, 0);
            totalPrice.innerHTML = total;
            cartBadge.innerHTML = shop.cartCounter;
            shoppingCart.classList.toggle("disabled", shop.cartCounter === 0);
            record.classList.toggle("cart-item-empty", cartItem.amount === 0);
            checkContinueButton();
          });
        });
      }

      function assignDeleteButtons() {
        const buttons = document.querySelectorAll(".item-delete");
        buttons.forEach((button) => {
          const id = button.dataset.itemId;
          const record = document.getElementById(`item${id}`);
          const input = document.querySelector(`input[name="quantity-item${id}"]`);
          button.addEventListener("click", (event) => {
            const value = Number(input.value);
            shop.cart = shop.cart.filter(item => item.id !== id);
            shop.cartCounter = shop.cartCounter - value;
            sessionStorage.setItem("shop", JSON.stringify(shop));
            checkContinueButton()
            input.value = 0;
            input.dispatchEvent(new Event("change"));
            record.remove();
          });
        });
      }

      function checkContinueButton() {
        const btn = document.querySelector(`#cartForm > button[type="submit"]`);
        btn.classList.toggle("disabled", shop.cartCounter === 0);
        if (shop.cart.length === 0) {
              sessionStorage.removeItem("shop");
            }

      }

      function formItemTemplate(item) {
        return `
      <li id="item${item.id}" class="cart-item card position-relative p-2">
            <div class="row g-0">
              <div class="col-2"><img src="${item.imgUrl}" alt="Goods" class="img-fluid rounded" /></div>
              <div class="col-10 card-body">
                <h5><a href="${item.url}" class="text-white">${item.name}</a></h5>
                <button type="button" class="item-delete btn btn-sm btn-dark btn-floating border-0 position-absolute" data-item-id="${item.id}">
                    <i class="fas fa-trash"></i>
                </button>
                <div class="item-quantity">
                  <button type="button" class="btn btn-sm btn-dark btn-floating border-0" data-item-id="${item.id}" data-action="minus" data-mdb-ripple-color="dark">
                    <i class="fas fa-minus"></i>
                  </button>
                  <input type="text" class="amount" name="quantity-item${item.id}" tabindex="-1" readonly="readonly" value="${item.amount}" min="0" />
                  <button type="button" class="btn btn-sm btn-dark btn-floating border-0" data-item-id="${item.id}" data-action="plus" data-mdb-ripple-color="dark">
                    <i class="fas fa-plus"></i>
                  </button>
                  <span class="price price-relative">${item.amount * item.price}</span>
                </div>
              </div>
            </div>
          </li>
      `;
      }

      function fillForm() {
        cart = shop.cart;
        let data = "";
        let total = 0;
        output = cart.forEach((item) => {
          data += formItemTemplate(item);
          total += item.amount * item.price;
        });
        const form = document.getElementById("cartForm");
        form.addEventListener("submit", formSubmit);

        const itemList = form.querySelector("ul");
        itemList.innerHTML = data;
        const totalPrice = form.querySelector("#total-price span");
        totalPrice.innerHTML = total;
      }

      function formSubmit(event) {
        event.preventDefault();
        const isLoggedIn = document.getElementById("navUserName") !== null;
        window.location.href = isLoggedIn ? "/purchase" : "/authprompt";
      }
