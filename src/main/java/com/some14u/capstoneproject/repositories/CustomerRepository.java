package com.some14u.capstoneproject.repositories;

import com.some14u.capstoneproject.models.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {
    boolean existsByLogin(String login);

    Customer findByLogin(String login);
}
