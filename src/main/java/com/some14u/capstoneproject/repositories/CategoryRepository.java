package com.some14u.capstoneproject.repositories;

import com.some14u.capstoneproject.models.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, Integer> {
}
