package com.some14u.capstoneproject.repositories;

import com.some14u.capstoneproject.models.Good;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GoodRepository extends JpaRepository<Good, Integer> {
    List<Good> findByCategory_Endpoint(String endpoint);

    List<Good> findByCategory_Endpoint(String endpoint, Pageable pageable);

    List<Good> findByCategory_Endpoint(String endpoint, Sort sort);
}
