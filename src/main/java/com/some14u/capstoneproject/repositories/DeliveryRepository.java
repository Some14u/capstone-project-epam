package com.some14u.capstoneproject.repositories;

import com.some14u.capstoneproject.models.Delivery;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface DeliveryRepository extends JpaRepository<Delivery, Integer> {
    List<Delivery> findByCustomer_IdOrderByDateDesc(Integer id);

    List<Delivery> findByStatusOrderByDateDesc(Delivery.DeliveryStatus status);

    List<Delivery> findByCustomer_IdAndStatusOrderByDateDesc(Integer id, Delivery.DeliveryStatus status);

    @Transactional
    @Modifying
    @Query("update Delivery d set d.status = ?1 where d.id = ?2")
    int updateStatusById(Delivery.DeliveryStatus status, Integer id);
}
