package com.some14u.capstoneproject.controllers;

import com.some14u.capstoneproject.components.DeliveryPositionSetFormatter;
import com.some14u.capstoneproject.components.LocalDateTimeFormatter;
import com.some14u.capstoneproject.models.Customer;
import com.some14u.capstoneproject.models.Delivery;
import com.some14u.capstoneproject.services.DeliveryService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@Controller
@RequestMapping(path = "/purchase")
@RequiredArgsConstructor
public class PurchaseController {
    private final DeliveryService deliveryService;
    private final DeliveryPositionSetFormatter deliveryPositionSetFormatter;
    private final LocalDateTimeFormatter localDateTimeFormatter;

    @GetMapping
    public String purchaseForm(Model model) {
        model.addAttribute("delivery", new Delivery());
        return "purchase";
    }

    @PostMapping
    public String purchase(@ModelAttribute("delivery") Delivery delivery, Authentication authentication) {
        Customer author = (Customer) authentication.getPrincipal();
        delivery.setStatus(Delivery.DeliveryStatus.PENDING);
        delivery.setDate(LocalDateTime.now());
        delivery.setCustomer(author);
        deliveryService.create(delivery);
        return "redirect:/";
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.addCustomFormatter(deliveryPositionSetFormatter);
        binder.addCustomFormatter(localDateTimeFormatter);
    }
}
