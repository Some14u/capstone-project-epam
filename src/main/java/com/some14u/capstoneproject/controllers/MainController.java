package com.some14u.capstoneproject.controllers;

import com.some14u.capstoneproject.services.CategoryService;
import com.some14u.capstoneproject.services.GoodService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
@RequiredArgsConstructor
public class MainController {
    private final CategoryService categoryService;
    private final GoodService goodService;

    @GetMapping(path = "/shop/{category}")
    public String getGoodsByCategory(@PathVariable(name = "category") String category, Model model) {
        var list = goodService.getByCategory(category);
        model.addAttribute("goods", list);
        model.addAttribute("categories", categoryService.getAll());
        model.addAttribute("currentCategory", category);
        return "shop";
    }

    @GetMapping(path = "/")
    public String redirectToShop() {
        return "redirect:/shop";
    }

    @GetMapping(path = "/shop")
    public String redirectToFirstCategory() {
        var firstCategory = categoryService.getByOrder(1);
        return "redirect:/shop/" + firstCategory.getEndpoint();
    }

    @GetMapping(path = "/about")
    public String about() {
        return "about";
    }

    @GetMapping(path = "/cart")
    public String cart() {
        return "cart";
    }

    @GetMapping(path = "/authprompt")
    public String authprompt() {
        return "authprompt";
    }
}
