package com.some14u.capstoneproject.controllers;

import com.some14u.capstoneproject.models.Customer;
import com.some14u.capstoneproject.services.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequiredArgsConstructor
public class CustomerController {
    private final CustomerService customerService;

    @GetMapping(path = "/register")
    public String registerForm(Model model) {
        var customer = new Customer();
        model.addAttribute("customer", customer);
        return "register";
    }

    @PostMapping(path = "/register")
    public String createNewCustomer(Customer customer, Model model, @RequestParam String password, HttpServletRequest request) {
        if (!customerService.create(customer)) {
            model.addAttribute("errorMessage", "Customer with login " + customer.getLogin() + " already exists");
            return "register";
        }
        try {
            request.login(customer.getLogin(), password);
        } catch (ServletException e) {
            System.out.println("Error while login " + e);
        }
        return "redirect:/";
    }

    @GetMapping(path = "/signin")
    public String signInForm() {
        return "signin";
    }
}
