package com.some14u.capstoneproject.filters;

import com.some14u.capstoneproject.services.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@RequiredArgsConstructor
public class MainFilter extends HttpFilter {
    private final CategoryService categoryService;

    @Override
    protected void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        final HttpServletRequestWrapper wrapped = new HttpServletRequestWrapper(request) {
            @Override
            public String getRequestURI() {
                var originalUri = ((HttpServletRequest) getRequest()).getRequestURI();
                var testString = removeLeadingSlash(originalUri);
                if (categoryService.getAll().stream().anyMatch(category -> category.getEndpoint().equals(testString))) {
                    return "/shop" + originalUri;
                }
                return originalUri;
            }

            private String removeLeadingSlash(String originalUri) {
                return originalUri.substring(1);
            }
        };
        chain.doFilter(wrapped, response);
    }
}