package com.some14u.capstoneproject.components;

import com.some14u.capstoneproject.models.DeliveryPosition;
import com.some14u.capstoneproject.services.GoodService;
import lombok.RequiredArgsConstructor;
import org.springframework.format.Formatter;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class DeliveryPositionSetFormatter implements Formatter<Set<DeliveryPosition>> {

    private final GoodService goodService;

    @Override
    @NonNull
    public Set<DeliveryPosition> parse(String text, @NonNull Locale locale) {
        var list = text.split(",");
        return Arrays.stream(list).map(record -> {
            var res = record.split("-");
            var id = Integer.valueOf(res[0]);
            var amount = Integer.valueOf(res[1]);
            return new DeliveryPosition(goodService.findById(id), null, amount);
        }).collect(Collectors.toSet());
    }

    @Override
    @NonNull
    public String print(@NonNull Set<DeliveryPosition> object, @NonNull Locale locale) {
        return "";
    }
}
