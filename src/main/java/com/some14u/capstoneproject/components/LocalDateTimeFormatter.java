package com.some14u.capstoneproject.components;

import org.springframework.format.Formatter;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

@Component
public class LocalDateTimeFormatter implements Formatter<LocalDateTime> {

    @Override
    @NonNull
    public LocalDateTime parse(@NonNull String text, @NonNull Locale locale) {
        return LocalDateTime.parse(text, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    }

    @Override
    @NonNull
    public String print(@NonNull LocalDateTime object, @NonNull Locale locale) {
        return object.toString();
    }
}
