package com.some14u.capstoneproject.models;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;
import java.util.Objects;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Embeddable
@EqualsAndHashCode
public class DeliveryPosition {
    @ManyToOne
    @JoinColumn(name = "good_id")
    private Good good;
    @Column(name = "purchased_price", nullable = false, precision = 10, scale = 2)
    private BigDecimal purchasedPrice = BigDecimal.ZERO;
    @Column(name = "amount", nullable = false)
    private Integer amount = 0;

    public DeliveryPosition(Good good, BigDecimal purchasedPrice, Integer amount) {
        this.good = good;
        this.purchasedPrice = Objects.isNull(purchasedPrice)
                ? BigDecimal.ZERO.add(good.getPrice())
                : purchasedPrice;
        this.amount = amount;
    }
}
