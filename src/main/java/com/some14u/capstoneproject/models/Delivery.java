package com.some14u.capstoneproject.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@Table(name = "deliveries")
public class Delivery {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "address", nullable = false)
    private String address;
    @Column(name = "status", nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private DeliveryStatus status;
    @Column(name = "date", nullable = false)
    @CreationTimestamp
    private LocalDateTime date;
    @Column(name = "expires", nullable = false)
    private LocalDateTime expires;
    @Column(name = "details")
    private String details;
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "customer_id", nullable = false)
    private Customer customer;

    @ElementCollection
    @CollectionTable(name = "delivery_positions", joinColumns = @JoinColumn(name = "delivery_id"))
    private Set<DeliveryPosition> deliveryPositions = new LinkedHashSet<>();

    public enum DeliveryStatus {
        PENDING, FULFILLED, REFUSED
    }
}
