package com.some14u.capstoneproject.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@Table(name = "goods")
public class Good {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "name", nullable = false, length = 50)
    private String name;

    @Column(name = "price", nullable = false, precision = 10, scale = 2)
    private BigDecimal price;

    @Column(name = "purchase", nullable = false, precision = 10, scale = 2)
    private BigDecimal purchase;

    @Column(name = "details")
    private String details;

    @Column(name = "units", length = 20)
    private String units;

    @Column(name = "supplier", length = 100)
    private String supplier;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "category_id", nullable = false)
    private Category category;

    @ElementCollection
    @CollectionTable(name = "good_images", joinColumns = @JoinColumn(name = "good_id"))
    @Column(name = "name")
    private Set<String> images = new HashSet<>();
}
