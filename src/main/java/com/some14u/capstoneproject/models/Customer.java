package com.some14u.capstoneproject.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.Collections;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@Table(name = "customers")
public class Customer implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Embedded
    private FullName name;
    @Column(name = "phone", nullable = false, length = 100)
    private String phone;
    @Column(name = "login", nullable = false, length = 100, unique = true)
    private String login;
    @Column(name = "password", nullable = false, length = 1000)
    private String password;
    @Column(name = "role", nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private CustomerRole role;
    @Column(name = "active", nullable = false)
    private Boolean active = true;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singleton(CustomerRole.ROLE_CUSTOMER);
    }

    @Override
    public String getUsername() {
        return getLogin();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return getActive();
    }

    public enum CustomerRole implements GrantedAuthority {
        ROLE_CUSTOMER, ROLE_ADMINISTRATOR, ROLE_EMPLOYEE;

        @Override
        public String getAuthority() {
            return name();
        }
    }
}
