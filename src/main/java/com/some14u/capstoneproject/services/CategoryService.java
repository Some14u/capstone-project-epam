package com.some14u.capstoneproject.services;

import com.some14u.capstoneproject.models.Category;
import com.some14u.capstoneproject.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service

public class CategoryService {
    private final List<Category> categoryList;

    @Autowired
    public CategoryService(CategoryRepository categoryRepository) {
        categoryList = categoryRepository.findAll(Sort.by("order"));
    }

    public List<Category> getAll() {
        return categoryList;
    }

    public Category getByOrder(Integer order) {
        return categoryList
                .stream()
                .filter(category -> category.getOrder().equals(order))
                .findFirst()
                .orElse(null);
    }
}
