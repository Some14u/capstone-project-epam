package com.some14u.capstoneproject.services;

import com.some14u.capstoneproject.models.Customer;
import com.some14u.capstoneproject.repositories.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CustomerService {
    @Autowired
    private final CustomerRepository customerRepository;
    @Autowired
    private final PasswordEncoder passwordEncoder;

    public Customer getByLogin(String login) {
        return customerRepository.findByLogin(login);
    }

    public boolean create(Customer customer) {
        var login = customer.getLogin();
        if (customerRepository.existsByLogin(login)) {
            return false;
        }
        customer.setActive(true);
        customer.setPassword(passwordEncoder.encode(customer.getPassword()));
        customer.setRole(Customer.CustomerRole.ROLE_CUSTOMER);
        customerRepository.save(customer);
        customerRepository.flush();
        System.out.println("Created new customer " + customer);
        return true;
    }


}
