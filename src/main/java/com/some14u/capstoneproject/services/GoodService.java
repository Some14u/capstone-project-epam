package com.some14u.capstoneproject.services;

import com.some14u.capstoneproject.models.Good;
import com.some14u.capstoneproject.repositories.GoodRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class GoodService {
    private final GoodRepository goodRepository;

    public List<Good> getByCategory(String category) {
        return goodRepository.findByCategory_Endpoint(category, Sort.by("name"));
    }

    public Good findById(Integer id) {
        return goodRepository.findById(id).orElse(null);
    }
}
