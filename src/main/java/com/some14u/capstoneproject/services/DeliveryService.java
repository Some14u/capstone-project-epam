package com.some14u.capstoneproject.services;

import com.some14u.capstoneproject.models.Delivery;
import com.some14u.capstoneproject.repositories.DeliveryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DeliveryService {
    private final DeliveryRepository deliveryRepository;

    public List<Delivery> getAllByCustomerId(Integer id) {
        return deliveryRepository.findByCustomer_IdOrderByDateDesc(id);
    }

    public List<Delivery> getAllHavingStatus(Delivery.DeliveryStatus status) {
        return deliveryRepository.findByStatusOrderByDateDesc(status);
    }

    public List<Delivery> getAllByCustomerIdHavingStatus(Integer id, Delivery.DeliveryStatus status) {
        return deliveryRepository.findByCustomer_IdAndStatusOrderByDateDesc(id, status);
    }

    public int setStatus(Integer id, Delivery.DeliveryStatus status) {
        return deliveryRepository.updateStatusById(status, id);
    }

    public void create(Delivery delivery) {
        deliveryRepository.saveAndFlush(delivery);
    }
}
